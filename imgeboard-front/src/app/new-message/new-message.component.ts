import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessagesService } from '../shared/messages.service';
import { MessageData } from '../shared/message.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.sass']
})
export class NewMessageComponent implements OnInit, OnDestroy {
  @ViewChild('f') messageForm!: NgForm;
  messageUploadingSubscription!: Subscription;
  isUploading = false;

  constructor(private messagesService: MessagesService) { }

  ngOnInit(): void {
    this.messageUploadingSubscription = this.messagesService.messagesUploading.subscribe((isUploading: boolean) =>{
      this.isUploading = isUploading;
    })
  }

  onSubmit(){
      const messageData: MessageData = this.messageForm.value;
      this.messagesService.createMessage(messageData);
      this.messagesService.getMessages();
      this.messageForm.resetForm();
  }

  ngOnDestroy() {
    this.messageUploadingSubscription.unsubscribe();
  }
}
