import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message, MessageData } from './message.model';
import { environment } from '../../environments/environment';
import { map, Subject, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  constructor(private http: HttpClient) {};

  messagesChange = new Subject<Message[]>();
  messagesUploading = new Subject<boolean>();
  messagesGetting = new Subject<boolean>();

  private messages: Message[] = [];

  getMessages(){

    this.messagesGetting.next(true);
    this.http.get<Message[]>(environment.apiUrl + '/messages').pipe(
      map(response =>{
      return response.map(messageData =>{
        return new Message(
          messageData.id,
          messageData.author,
          messageData.message,
          messageData.datetime,
          messageData.image,
        );
      });
    })
    ).subscribe(messages =>{
      this.messages = messages;
      this.messagesChange.next(this.messages.splice(0));
      this.messagesUploading.next(false);
    })
  }

  createMessage(messageData: MessageData) {
    this.messagesUploading.next(true);
    const formData = new FormData();
    Object.keys(messageData).forEach(key => {
      if (messageData[key] !== null) {
        formData.append(key, messageData[key]);
      }
    });
    this.http.post<Message>(environment.apiUrl + '/messages', formData).subscribe({
      next: () => {
        this.getMessages();
        this.messagesUploading.next(false);
      },
      error: (() => {
        this.messagesUploading.next(false);
      })
    });
  }

}
