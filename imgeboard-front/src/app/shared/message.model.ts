export class Message {
  constructor(
    public id: string,
    public author: string,
    public message: string,
    public datetime: string,
    public image: string,
  ){}
}

export interface MessageData {
  [key: string]: any;
  author: string;
  message: string;
  datetime: string;
  image: File | null,
}
